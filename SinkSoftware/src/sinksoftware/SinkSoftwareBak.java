package sinksoftware;


import javax.swing.JOptionPane;

public class SinkSoftwareBak{


    
    //Gets an instance of the MainFrame. 
    private static MainFrame gameFrame = new MainFrame();
    private static Menu menuFrame = new Menu();
    private static Sound gameMusic;
    
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Put file name of menu music here.
        //To do sound effects put in SFX file and do SFX/nameoffile.wav
        Sound menuMusic = new Sound("Music/Menu.wav");
        //Got to call play for it to play.
        menuMusic.loop(100);
        menuMusic.play();
        
       
        while (menuFrame.menuOn){
            menuFrame.setVisible(true);
            
        }
        
        menuFrame.setVisible(false);
        gameFrame.setVisible(true);
      
        gameFrame.setBackground("Images/background.jpg");
        
        //If image is greater than (250 width 300 height) things will get cut off.
        //I tried coding in something to just down size it in the method but
        //that only mamkes it seem squished together(the oposite effect). 
        //Just before you add an image make sure it's dimensions are atleast (250,300)x,y resolution or less. 
   
        

        //Menu music stoped.
        menuMusic.stop();
        

        //Game music is a field so we can call it any where in our code unlike menuMusic.
     
     

        //Put this before or after play. Or just don't call it if you only want too play sound/music once.
 

        //Calls our play method down at the bottom.
        play();
       
        
    }
    
  

    //This game method makes it a little easier for use to change the text in outputTextArea. This way we don't have to deal with the field directly.
    private static void setText(String dialog, String options){
        gameFrame.outputTextArea.setText(dialog + options);
    }

    //Praise the Lord I got this working. Litterly took me hours and total stress. 
    //looking for a solution through hundreds of forms. Even posted on one and no one replied. 
    //You don't have to worry about this code though I left comments. Just be glad it's here. 
    private static int getUserInput(int maxOption){
        
        boolean keepRunning = true;
 
        while(keepRunning == true){
            gameFrame.userChoiceInt = 0;
            while ( gameFrame.userChoiceInt == 0){

                //This refreshes the gameFrame so it checks for the submit button getting clicked other wise without this statment
                //it would be an infinite loop. Dirty? Yes. Works? Totally :D. This fixed all of our problems so leave it alone. Please :)
                gameFrame.refresh();
         
            }
            
            
            if (gameFrame.userChoiceInt > maxOption || gameFrame.userChoiceInt <= 0 ){
                JOptionPane.showMessageDialog(null, "You can only enter numbers 1 through " + String.valueOf(maxOption),"User Error", JOptionPane.INFORMATION_MESSAGE);
                gameFrame.userInputTextField.setText("");
            }else{
                keepRunning = false;
                
            }
        }
        //Erase text.
        gameFrame.userInputTextField.setText("");
        
        return gameFrame.userChoiceInt;
    
        }
        
   
    
    
    
    //STORY HERE!!!
   //This is where most the story code will go. We can can call other methods from inside if need be to make it less BIG cause it's going to get BIG and hard to read.
    //You guys can do what you want with in this method. Don't worry I will help you and am open for improvments.

    
        
private static void play(){
          
        
       
        
        int userInput;
        
        //So there are three steps to making a branch. Here they are. We just
        //keep doing these three steps until we are out of story or are hands, eyes, and sanity are all shot.
        
        //Step 1. Display message.
        //There are twp arguments. The first is for the instructions. The other is for the options. I thought this made it cleaner/clearer than just one huge string.
        setText("Commander's log entry 3224. Arrival on Xenoliphus IV. \nAfter pursuing the ancient artifact \nof the Skrejgib for over 14 Earth years, \nI am finally making progress. Probably. \nAfter I, well, borrowed the Koob of Sterces from \nBladimir four, it seems to \npoint to this backwater planet. \nSo now I'm on this rathole in the unfashionable \neastern arm of the galaxy. But as an interstellar\n mercenary, I've seen worse. Twice actually.\nNow that I'm here in Wen Kyor Ytic spaceport, \nI need to decide where to go", "\n1.Straight to the Xelaphigan Swamp, \nwhere the next clues are to be found \n2. To seek the wisdom of the great Adoy. \n3. To the mercantile exchange region.");


        //Step 2. Wait for userInput and set it equal to the local variable userInput when we declared.
        //We pass in a number 3 in this case cause, for the max options the user has. This way the user doesn't enter an option that isn't available. Like -4, 0, or 12 for example.
        //BTW 0 is not an option nor will we ever allow it to be. 1 is where we start!. 
        userInput = getUserInput(3);


        //Step 3. Check userInput and decide what to do based on what the user entered.
        //We had declared 3 optoins for the user so we have three if statments. userInput == 1 userInput ==3 or userInput == 3
        //I would also mark how far in we are with numbers so you make less mistakes.
        //1
        if (userInput == 1){
                 Sound swampMusic = new Sound("Music/Swamp.wav");
              swampMusic.play();
              gameFrame.setRelatedImage("Images/Jungle.png");
              setText("I'm feeling bold. Onward to the swamp! \n---------------------------------------------------------"
                      + "\nWell look who's here. The fabled swamp monster, Rodrom.\nI didn't think he actually exists.\nHe seems angry. Something about defiling holy ground.",
                      "\nChoices:\n1. Outwit Rodrom with clever use of words.\n2. Use my late grandfather's favorite tactic: LASER to the FACE!");
              userInput = getUserInput(3);
              
              
              //2
              if(userInput == 1){
                  setText("I think I upset him, because he just tore off my left arm.", "\n1. Apply my only uberbandage, and RUN! \n2. Bleed out and die.");
                  userInput = getUserInput(2);
                  
                  //3
                  if (userInput == 1){
                       setText("Rodrom doesn't seem interested in chasing, which I think is a good thing. \nNow I'm wondering where I am. I guess further into the bog is enough info for now. \nAlso, what's that grunting sound? ","\n1. Stay far away from it\n2. Walk heedlessly toward it\n3. Use my seeker drone to check it out.");
                  
                       
                        userInput = getUserInput(3);
                  //3
                  }else if(userInput == 2){
                      setText("RIP me, until the next life...", "");
                  }
                  
                    //4
                    if (userInput == 1){
                        setText("I'll just keep walking for now. \nMaybe I can double back and get back to Wen Kroy Ytic\nand try another cour--- \nOh my. So that's the grunting sound. A 12' tall Nomekop.","\n1. Draw my weapon and fire upon the Nomekop \n2. Explain to him that I'm an only child, and my mother was really\nhoping that I would return to law school and earn my degree,\nand I can't do that if he kills me.");
                    userInput = getUserInput(3);
                    
                        //5
                        if (userInput == 1){
                            setText("We had a truly epic fight. I fatally wounded him. \nUnfortunately, he also fatally wounded me.\nMy only uberbandage has already been used.\nWhat a lame day this is turning out to be.","1. Send a final message to mother.\n2. Reflect upon my life.");
                            userInput = getUserInput(2);
                            
                            //6
                            if (userInput == 1){
                                setText("Dear mother. Try to remember that my life of vice \nisn't your fault.\nRather, it's a result of complex biological and sociological factors\nover which you had little control.\nGoodbye Mamma.","");
                            }else if (userInput == 2){
                                setText("I'm not gonna lie, this reflection thing\nis a mistake. But at least\nit will all be over soon.","");
                            }
                            
                        }else if(userInput ==2){
                            setText("The Nomekop nods in understanding, and tells me that \nhe thinks I would make a great lawyer. And then he offers to take me to the artifact I seek!\nI eagerly open it up with my \nintergalactic notoleks key. And I'm sad to realize that\n whatever it is must be highly radioactive, because\n my face is melting. That's life.","");
                        }
                    //4    
                    }else if(userInput == 2){
                        setText("Oh my. It's a 12' tall Rodrom. He's picked me up and is now going to eat me. This would be a learning experience, except that I won't survive","");
                       
                    }else if (userInput == 3){
                        setText("My seeker drone has found that there's a 12' tall Rodrom hiding in the grove of Enip trees.\nHe devoured my drone, which is rude,\nbut at least I can avoid him if I want.","\n1. Avoid him.\n2.Prove I'm a man. Make that Rodrom repay me for breaking my drone!");
                    userInput = getUserInput(3);
                    
                        //5
                        if (userInput == 1){
                            setText("As I pass safely away from the Rodrom, \nI feel an voice from within my inner conciousness telling me where to go.","\n1. Listen to the voice.\n2. Ignore it. I'm not superstitious.");
                            userInput = getUserInput(2);
                            
                            //6
                            if (userInput == 1){
                                setText("I've never believed in all that spiritualistic tihsllub, \nbut here I am, beneath the big Aybud that the artifact described","\n1. Keep listening in case the inner voice speaks again\n2. Pry open the ancient chest and find the treasure I have long sought!");
                            //6
                            userInput = getUserInput(2);
                            }else if(userInput ==2){
                                setText("Well I've been wandering through the desert for 5 days now, and my compass doesn't seem to work on this planet. Here I am, down to my final choice.","\n1. Jump off the cliff\n2. Die of thirst");
                                userInput = getUserInput(2);
                                //7
                                if (userInput ==1){
                                    setText("YAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHH!!!!!!\n*splut*","");
                                    
                                }
                                //7
                                else if (userInput == 2){
                                    setText("I see a light. I see the next life. \nAnd now I feel sorrow for the vile life I've lived.\nFor I see the reward of my wickedness.\nThe angels are coming to take me to Cleveland!","");
                                }
                            }
                        }
                            
                        //5
                        else if(userInput ==2){
                            setText("He's picked me up and is now going to eat me. This would be a learning experience, except that I won't survive","");
                            
                        }    
                    }
                    
                  
              //2
              }else if(userInput == 2){
                  setText("The good news is that I blinded him in one eye.\nThe bad news is that my laser is still recharging, and he's\nglaring at me with his other 5 eyes.\nI do believe this is the end of the line for me.\nCome to think of it, Grandfather only used that trick once, didn't he?","");
                  userInput = getUserInput(3);
                
                  
                  
               //2
              }else if(userInput == 3){
                  setText("That was fun! But come one let's buy a ship. What do you want?", "\n1. Millennium Falcon. \n2. Starship Enterprise");
                  userInput = getUserInput(2);
              }
              

              
              
        //1
        }else if(userInput == 2){
            setText("I ask Adoy if he is willing to help me with my quest. He nods \nand tells me.\"You bet. We'll talk about it \nover a beer. And you're buying.\"","\n1. Accept and head for the Space Bar.\n2. Politely decline. I don't have money to waste on this old\ntimer's drinking habit.");
            userInput = getUserInput(2);
            //2
            if (userInput == 1){
                setText("Adoy just talks and talks about \nhow he was such a great llabtoof \nplayer back in the day. Nothing you say \ncan get him to talk about what you need.","\n1. Politely explain to him that you don't want to keep talking about\nhis past.\n2. Impolitely explain to him that you don't want to keep talking about his past.");
                if (userInput == 1){
                    setText("Adoy nods and explains that he understands.\nBut he simply doesn't care about my opinion.\nOr appreciate my rudeness, for that matter. So now here I am.\nHe trapped me in an alternate universe. And I can't see anything\nexcept endless replays of his llabtoof career.\nWhat a downer.","");
                }else if(userInput ==2){
                    setText("Adoy looks very displeased. And now\nhe's shooting lasers out of his\neyes. I didn't know he could do that.\nNow I know, but I don't think it's gonna do me any good.","");
                    
                }
            }
            
            
       
        //1
        }else if(userInput == 3){
            setText("You are at the market place. Your options are:","\n1. Buy a gun. \n2. Buy food.");
          userInput = getUserInput(2);
          if (userInput == 1){
              setText("You bought a revolver. Now what are you going to do?","\n1. Put it away and move about your business. \n2. Rob the market.");
              userInput = getUserInput(2);
              if (userInput == 1){
                  setText("He thanks me for buying the gun. \nNow get outta here! Unfortunately, I was ambushed by\nseparatist forces on the way out.\nThey set me on fire.\nWhat a terribly unfortunate experience this has become.","");
                  
              }else if (userInput == 2){
                  setText("A guard sees you try to rob the marketplace and shoots you. You died!","");
              }
          }else if(userInput == 2){
              setText("You bought food, now what?","\n1. Eat it. \n2. Walk away. \n3. Throw it at a guard.");
              userInput = getUserInput(3);
              if (userInput == 1){
                  setText("Delicous! But it turns out it's poisoned. Nmad.","");
              }else if(userInput == 2){
                  setText("You move on to the bar. The Market owner says goodbye. And then he shoots you in the back for no reason.","");
              }else if(userInput == 3){
                  setText("The guard was not too happy with your actions \nand you get a one way ticket to jail, complete\n with a free chokehold from the guard, have fun!","");
              }
        }
        }     
         
        
        //Yep guys this is going to be long...
         
    }
        
        
        
}



