
package sinksoftware;


import javax.sound.sampled.*;




public class Sound {
    
    
    private Clip clip;
  
    public Sound(String file){
  
        
        try{
            
            AudioInputStream ais = AudioSystem.getAudioInputStream(getClass().getResourceAsStream(file));
            
            AudioFormat baseFormat = ais.getFormat();
            
            AudioFormat decodeFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 
                    16,baseFormat.getChannels(),baseFormat.getChannels() * 2, 
                    baseFormat.getSampleRate(),false);
            
            AudioInputStream dais = AudioSystem.getAudioInputStream(decodeFormat, ais);
            clip = AudioSystem.getClip();
            clip.open(dais);
            
        }catch(Exception e){
           
        }
        
        
       
    }
        public void play(){
            try{
                if (clip == null) return;

                stop();
                clip.setFramePosition(0);
                clip.start();

            }catch(Exception e){


            }
            
        }
        
        public void stop(){
           try{
            
               if (clip.isRunning()) clip.stop();

            }catch(Exception e){
                   
            }
            
           
            
           
        }
        
        public void loop(int numberOfLoops){
            try{
                clip.loop(numberOfLoops);
            }catch(Exception e){
                    
    
            }
     
           
        }

      
        public void close(){
            
            try{
                stop();
                clip.close();
            }catch(Exception e){
       
            
    
            }
            
        }
        
}
    